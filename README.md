## Epam python course:  Final testing

Here you will fond records of my work on the project to simplify further protection. English, Motherf*cker, do you speak it? I'm not much, so I'll try not to get shot in the shoulder like poor Brett.
In this file you can find: explanation of the program logic, illustrations, references to the used literature

![enter image description here](https://pp.userapi.com/c848620/v848620284/84b7e/zNHE1Kj8Xms.jpg)

## Simulator of the robot moving on the field

# Task
Необходимо написать консольное интерактивное приложение, принимающее простой набор команд для управления роботом (точкой на поле) в рамках некоторого поля произвольного размера.

Минимальная функциональность:
1. Программа запускается из консоли и принимает одну из команд на вход
2. Возможные команды: вверх/вниз, влево/вправо, поворот на 90/180
3. Робот перемещается по полю X на Y, которые можно задать при запуске программы
4. При запуске программы на поле генерируется N препятствий (тоже задается как параметр), расположенных случайно: через них робот проехать не может
5. Робот стартует по центру и после каждой команды докладывает о своей позиции: (X, Y), куда повернут; откуда приехал
6. Виртуальное окружение со всеми необходимыми зависимостями, а также описание архитектуры и общей логики работы приложения

Дополнительная функциональность:
· Препятствия могут быть различного размера (случайно
· (*) Отображение текущей позиции робота выдавать в ascii графике с отрисовкой направления и близлежащих объектов в некотором радиусе, к примеру:
![enter image description here](https://pp.userapi.com/c848620/v848620284/84baa/WemtluNu-2M.jpg)

· Задействовать ascii цвета для вывода в терминал
· Хранить и позволять экспортировать результирующий маршрут робота в файл (json или любой другой формат)
· Написать набор тестов для основных методов приложения

На финальную оценку будет влиять работоспособность программы, реализованная дополнительная функциональность, документация и стилистика кода.

Рекомендуется задействовать утилиты автоматического форматирования кода (спросить, если что непонятно) и писать осмысленные комментарии к основным методам программы.

# First step: let`s make field
To create a field, the following parameters are required from the user:
·  the dimensions of the field X and Y
· N obstacles


After considering several options for visualizing the field drawing, it was decided to consider the field as a matrix, the element of which can be an obstacle, an empty symbol, a robot or a wall. 


The characters are taken according to [the encoding](http://www.industrialnets.ru/files/misc/ascii.pdf) 
and used with help from [here.](http://pythonlearn.ru/stroki-python/kodirovanie-strok-simvolov-ascii-v-python/)

![enter image description here](https://sun9-1.userapi.com/c830409/v830409472/1ac3c6/PxsVSO96NZM.jpg)

# Second step: placing obstacles
First of all, I need to decide what restrictions will be imposed on the placement. I  found logical an option in which barriers are filled no more than half of the area of the field.Then it is necessary to solve the problem of space distribution on obstacles. 
Suppose we have a field of 5x5 and 5 obstacles, then the obstacles can take 12 cells distributed between each other randomly so that each will have a size of at least 1. At this stage we will use random. We obtain the following distribution:
case: random took maximum
| №	 | max random	 |  result |
|--|--|--
|  0| 12-5 | 7
|1|5-4|1
| 2 | 4-3 |1 
|3|3-2|1
| 4 | 2-1 |1 
|5|1|1

In programm, for example:
![enter image description here](https://pp.userapi.com/c847217/v847217913/f7c95/i2VqGhCn_cw.jpg)

The algorithm starts with an empty field, the output of which was given earlier.


We already have some list of values of obstacles. For clarity, I will give pictures for the already considered example [7, 2, 1, 1, 1].

The algorithm works based on the fact that the field is considered as a set of points. Each point has "neighbors": top, right, bottom, left. The obstacle will be considered "one" if all its components are neighbors directly or indirectly.

![enter image description here](https://pp.userapi.com/c844722/v844722187/ff756/ceqlLD7bwfQ.jpg)

*All that is described below as random-obtained by random

![enter image description here](https://pp.userapi.com/c852032/v852032787/13d21/D5Cwflk17Os.jpg)


Let's start building a size 7 obstacle.
To control the construction path, we will keep a kind of diary-storage in which the point that is part of the obstacle will be compared to the list of relations with neighbors (from the top to the left clockwise), initially filled with zeros TC unknown.
Select a free random point on the map.
Let it be a point (4,4).

storage

(4,4): [0, 0, 0, 0]

Randomly determine the direction where we will move and build:
- up-the point is already occupied, note it in the " map"

storage

(4,4): [-1, 0, 0, 0]

Randomly select another point
![enter image description here](https://pp.userapi.com/c852032/v852032787/13d28/8lLd6k0aPxU.jpg)


- right-point busy

(4,4): [-1, -1, 0, 0]

Repeat
- down-the point is free, write the path to the map and go to the new point

(4,4): [-1, -1, 1, 0]

(4, 3): [0, 0, 0, 0]

![enter image description here](https://pp.userapi.com/c852032/v852032787/13d36/Qi1J-8NV5ak.jpg)

Not always with this algorithm we will get a point from which we can build further. Since it is guaranteed to be built obstacle size 1+, then stop the construction in this situation.

![enter image description here](https://pp.userapi.com/c852032/v852032787/13d5d/tvWm1d0wVSE.jpg)
 

Done

If we built an obstacle-all is well and you can move on to another ? Almost, but you need to make one point to the obstacle superimposed on each other and not United in one thing - we will put urchins.
They will not allow the next building to consider the cell free.
 
 ![enter image description here](https://pp.userapi.com/c851436/v851436917/12df9/sbG2-XGMmJY.jpg)
 
 Once the required number of obstacles will be built-remove all unnecessary (hedgehogs-barriers) and get only free space in the four walls, the robot in the center and the fence.
Now I will give a flowchart of the algorithm after its implementation in Python:

![enter image description here](https://pp.userapi.com/c846416/v846416122/fb6eb/68njA-FFBk0.jpg)

# The third step: control the robot

If need to turn - turn, 
if need to move - check whether the robot is turned in the right direction, whether the cell is free, if the conditions are met - move, 
after the command is displayed " robot report"

![enter image description here](https://pp.userapi.com/c849420/v849420216/871f3/zX4GSzsPQug.jpg)
