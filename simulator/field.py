# making field
import random
from termcolor import colored

class field_c:
    # let's describe the created field and define the necessary methods
    def __init__(self, x, y, n):
        self.x = x
        self.y = y
        self.n = n
        self.picture = []
        self.storage = []
        self.robot = ()
        self.to_clear = set()
        self.last_pos = ()
        self.way = []


    def make_colored(self, ch):
        chars = {
            chr(9688): ("magenta", []),
            chr(9650): ("red", []),
            chr(9658): ("red", []),
            chr(9660): ("red", []),
            chr(9668): ("red", []),
            '\x23': ("cyan", []),
            '\x20': ("green", []),
        }
        default = ("red", [])
        color, attrs = chars.get(ch, default)
        return colored(ch, color, attrs=attrs)

    def print_colord_f(self):
        if self.y > 33:
            y1, y2 = self.robot[0]-16, self.robot[0]+16
            if y1 < 0:
                y1 = 0
            if y2 > self.y+1:
                y2 = self.y
        else:
            y1, y2 = 0, self.y
        if self.x > 100:
            x1, x2 = self.robot[1]-45, self.robot[1]+45
            if x1 < 0:
                x1 = 0
            if x2 > self.x+1:
                x2 = self.x
        else:
            x1, x2 = 0, self.x
        line, picture = '', []
        for i in range(y1, y2 + 2):
            for j in range(x1, x2 + 2):
                line += self.picture[i][j]
            picture.append(line)
            line = ''
        for src_line in picture:
            dst_line = ""
            i = 0
            chnum = len(src_line)
            while i < chnum:
                ch = src_line[i]
                dst_line = dst_line + self.make_colored(ch)
                i += 1
            print(dst_line)
        return 0

    def fill_lines(self, a, b):
        for i in range(min(a), max(a)+1):
            for j in range(min(b), max(b)+1):
                x, y = j, i
                if self.picture[y][x] == '\x23':
                    if self.picture[y][x - 1] == '\x20':
                        self.picture[y][x - 1] = '\x2B'
                        self.to_clear.add((y, x-1))
                    if self.picture[y][x + 1] == '\x20':
                        self.picture[y][x + 1] = '\x2B'
                        self.to_clear.add((y, x + 1))
        return 0

    def fill_columns(self, c, l):
        for i in range(min(c), max(c) + 1):
            for j in range(min(l), max(l) + 1):
                x, y = i, j
                if self.picture[y][x] == '\x23':
                    if self.picture[y - 1][x] == '\x20':
                        self.picture[y - 1][x] = '\x2B'
                        self.to_clear.add((y-1, x))
                    if self.picture[y + 1][x] == '\x20':
                        self.picture[y + 1][x] = '\x2B'
                        self.to_clear.add((y + 1, x))


        return 0

    def put_sheald(self):
        """put a barrier at an obstacle that there was no layering"""
        c, l = {s[0][0] for s in self.storage}, {s[0][1] for s in self.storage}
        # go on line
        assert self.fill_lines(l, c) == 0
        # same with colummns
        assert self.fill_columns(c, l) == 0
        return 0

    def obstacle_start(self):
        """getting the initial coordinate for the building"""
        while True:
            start_point = (random.randint(1, self.x), random.randint(1, self.y))
            if self.picture[start_point[1]][start_point[0]] == '\x20' and start_point != self.robot:
                break
        return [start_point[0], start_point[1]]

    def move_point(self, j, p):
        """change the coordinate according to the command"""
        if j not in range(0, 4):
            return -1
        if j == 0:  # up
            p[1] = p[1] - 1
        elif j == 1:  # right
            p[0] = p[0] + 1
        elif j == 2:  # down
            p[1] = p[1] + 1
        elif j == 3:  # left
            p[0] = p[0] - 1
        return p

    def free_place_check(self, i, j, point):
        """check on the possibility of using"""
        p = point.copy()
        p = self.move_point(j, p)
        if self.storage[i][1][j] == 0 and self.picture[p[1]][p[0]] == '\x20':
            self.storage[i][1][j], point = 1, p
        else:
            self.storage[i][1][j] = -1
        return point

    def clear(self):
        """remove extra characters"""
        for point in self.to_clear:
            self.picture[point[0]][point[1]] = '\x20'
        return 0

    def place_obstacle(self, size):
        """place an obstacle on the field"""
        # now start painting
        placed, start_again = 0, 1  # flags
        while not placed:
            self.storage = []
            c = self.obstacle_start()
            for i in range(0, size):
                self.picture[c[1]][c[0]] = '\x23'  # made '#'
                self.storage.append((c, [0, 0, 0, 0]))
                directions = [0, 1, 2, 3]
                if i+1 == size:
                    placed, working_with_unit = 1, 0
                else:
                    working_with_unit = 1

                while working_with_unit:  # trying to finde plaсe for next unit
                    way = random.choice(directions)
                    directions.remove(way)
                    new_c = self.free_place_check(i, way, c)
                    if self.storage[i][1] == [-1, -1, -1, -1]:
                        break
                    if self.storage[i][1][way] == 1:
                        c = new_c
                        working_with_unit = 0
        assert self.put_sheald() == 0
        return 0

    def obstacles(self):
        """fill the field with obstacles with random size"""
        # only 50% of field can be obstacles and size of one at least 1
        free_space = (self.x*self.y)//2
        size_lst = []
        for item in range(0, self.n):
            size = random.randint(1, free_space-(self.n-1-item))
            size_lst.append(size)
            free_space -= size
        size_lst = sorted(size_lst, reverse=True)
        for size in size_lst:
            self.place_obstacle(size)
        self.clear()
        return 0

    def center_search(self, value):
        """ middle point search"""
        if not type(value) == int:
            return -1
        if value == 1:
            return 1
        elif value%2 == 0:
            return value//2
        else:
            return value//2+1

    def place_robot(self):
        """ place the robot in the center of the field"""
        # need to find the center and put the robot
        x = self.center_search(self.x)
        y = self.center_search(self.y)
        if x < 0 or y < 0:
            return -1
        self.robot = (y, x)
        self.way.append((y, x))
        self.picture[y][x] = chr(9650)
        return 0

    def make_box(self):
        """the marking of boundaries and the filling with empty points"""
        for i in range(0, self.y + 2):
            if i == 0 or i == self.y + 1:
                line = [chr(9688) for i in range(0, self.x + 2)]
            else:
                line = ['\x20' for i in range(1, self.x + 1)]  # then change to nan
                line.insert(0, chr(9688))
                line.append(chr(9688))
            self.picture.append(line)
        return 1

    def draw(self):
        """ draw a field """
        if self.make_box():
            assert self.place_robot() == 0
            assert self.obstacles() == 0
        else:
            print('Не получилось построить поле')
        self.print_colord_f()
        return 0

