# Тут будем вызывать все составляющие: поле, робота и остальное(если понаобится),
# русский текст - для пользователя, английский для дебага
import field
import sys
import os
import json



def get_parameter(a, text, max = sys.maxsize):
    a = ''
    while not a.isdigit() or (a.isdigit() and max<int(a)):
        if a.isdigit() and max<int(a):
            print('Столько препятствий не поместиться на поле')
        a = input(text)
    return int(a)


def start(x=10, y=10, n=5):
    '''start making field'''
    nice_place = field.field_c(x, y, n)
    nice_place.draw()
    return nice_place


def custom():
    """get the parameters to create the field"""
    x, y, n = 'unknown', 'unknown', 'unknown'
    initial_X = 'Размер поля по горизонтали: '
    initial_Y = 'Размер поля по вертикали: '
    initial_N = 'Количество препятствий на поле: '
    some_field = []
    x = get_parameter(x, initial_X)
    y = get_parameter(y, initial_Y)
    n = get_parameter(n, initial_N, (x * y) // 2)
    return x, y, n


def do_move(c, np):
    """move the robot in accordance with с"""
    if c == 'вверх':
        np.picture[np.robot[0] -1][np.robot[1]] = np.picture[np.robot[0]][np.robot[1]]
        np.last_pos = (np.robot[0], np.robot[1])
        np.picture[np.robot[0]][np.robot[1]] = '\x20'
        np.robot = (np.robot[0]-1, np.robot[1])
    elif c == 'вниз':
        np.picture[np.robot[0]+1][np.robot[1]] = np.picture[np.robot[0]][np.robot[1]]
        np.last_pos = (np.robot[0], np.robot[1])
        np.picture[np.robot[0]][np.robot[1]] = '\x20'
        np.robot = (np.robot[0] + 1, np.robot[1])
    elif c == 'влево':
        np.picture[np.robot[0]][np.robot[1]-1] = np.picture[np.robot[0]][np.robot[1]]
        np.last_pos = (np.robot[0], np.robot[1])
        np.picture[np.robot[0]][np.robot[1]] = '\x20'
        np.robot = (np.robot[0], np.robot[1]- 1)
    elif c == 'вправо':
        np.picture[np.robot[0]][np.robot[1]+1] = np.picture[np.robot[0]][np.robot[1]]
        np.last_pos = (np.robot[0], np.robot[1])
        np.picture[np.robot[0]][np.robot[1]] = '\x20'
        np.robot = (np.robot[0], np.robot[1] + 1)
    np.way.append(np.robot)
    return 0


def current_xy(np):
    return '(' + str(np.robot[1])+','+ str(np.robot[0]) + '), '


def c_from(np):
    if np.last_pos == ():
        return ''
    else:
        return '(' + str(np.last_pos[1]) + ',' + str(np.last_pos[0]) + ')'


def current_turn(np):
    """report on the current turn"""
    if np.picture[np.robot[0]][np.robot[1]] == chr(9650):
        return 'вверх, ', 0
    elif np.picture[np.robot[0]][np.robot[1]] == chr(9658):
        return 'вправо, ', 1
    elif np.picture[np.robot[0]][np.robot[1]] == chr(9668):
        return 'влево, ', 3
    elif np.picture[np.robot[0]][np.robot[1]] == chr(9660):
        return 'вниз, ', 2
    else:
        return 'что-то не так, ', -1

def robot_report(np):
    """the report on the position of the robot"""
    report = current_xy(np)
    m, r = current_turn(np)
    report += m
    report += c_from(np)
    return report


def do_turn(c, np):
    """turn the robot in accordance with с"""
    turns = (chr(9650), chr(9658), chr(9660), chr(9668))
    r, t = current_turn(np)
    if '90' in c:
        np.picture[np.robot[0]][np.robot[1]] = turns[(t+1)%4]
    elif '180' in c:
        np.picture[np.robot[0]][np.robot[1]] = turns[(t + 2) % 4]
    else:
        print('Error')
        return -1
    return 0


def can_move(c, np):
    """check whether you can take a point"""
    if c == 'вверх':
        if np.picture[np.robot[0]-1][np.robot[1]] == '\x20':
            return 1
        else:
            return 0
    elif c == 'вниз':
        if np.picture[np.robot[0]+1][np.robot[1]] == '\x20':
            return 1
        else:
            return 0
    elif c == 'влево':
        if np.picture[np.robot[0]][np.robot[1]-1] == '\x20':
            return 1
        else:
            return 0
    elif c == 'вправо':
        if np.picture[np.robot[0]][np.robot[1]+1] == '\x20':
            return 1
        else:
            return 0
    else:
        return -1


def corresponding_direction(c, np):
    """check if can move in the direction with the current direction"""
    r, t = current_turn(np)
    ways = ['вверх', 'вправо', 'вниз', 'влево']
    if c == ways[t]:
        return 1
    else:
        return -1


def simulation():
    new_field = input('Приветствую, перед вами симулятор робота перемещающегося по полю\n'
                      'Будет создано поле размером 20х20 с 5 препятствиями\n'
                      'Если вы хотите создать поле со своими параметрами - введите "1"\n')
    clear = lambda: os.system('clear')
    if new_field == '1':
        x, y, n = custom()
    else:
        x, y, n = 20, 20, 5
    np = start(x, y, n)
    print('Поле построено, можете давать команды\nТекущее положение робота:', robot_report(np))
    work, commands = 1, {'вверх', 'вниз', 'влево', 'вправо', 'поворот на 90', 'поворот на 180', 'стоп'}
    while work:
        command = ''
        while command not in commands:
            command = input('Возможные команды: вверх/вниз, влево/вправо, поворот на 90/180, стоп\n').lower()
        clear()
        os.system('clear')
        if command == 'стоп':
            work = 0
        else:
            if command == 'поворот на 90' or command == 'поворот на 180':
                do_turn(command, np)
            else:
                if corresponding_direction(command, np):
                    if can_move(command, np):
                        assert do_move(command, np) == 0
                    else:
                        print('Сюда ехать нельзя')
                else:
                    print('Попробуйте развернуться')
            print(robot_report(np))
            np.print_colord_f()
    with open('way.txt', 'w') as outfile:
        json.dump(np.way, outfile)
    return 0


if __name__ == '__main__':
    simulation()