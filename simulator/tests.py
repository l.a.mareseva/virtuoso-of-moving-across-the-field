import simulator as sm
import pytest
import field as fd


class TestSimulator:

    def test_can_move(self):
        model = fd.field_c
        model.picture, model.robot  = [[chr(9688), chr(9688), chr(9688), chr(9688), chr(9688)],
                              [chr(9688), '\x23', '\x20', '\x20', chr(9688)],
                              [chr(9688), '\x20', chr(9650), '\x20', chr(9688)],
                              [chr(9688), '\x20', '\x20', '\x23', chr(9688)],
                              [chr(9688), chr(9688), chr(9688), chr(9688), chr(9688)]], (2, 2)
        assert sm.can_move('вверх', model) == 1
        assert sm.can_move('влево', model) == 1
        assert sm.can_move('qq', model) == -1
        model.robot = (1, 0)
        assert sm.can_move('вверх', model) == 0

    def test_corresponding_direction(self):
        model = fd.field_c
        model.picture, model.robot = [[chr(9688), chr(9688), chr(9688), chr(9688), chr(9688)],
                                      [chr(9688), '\x23', '\x20', '\x20', chr(9688)],
                                      [chr(9688), '\x20', chr(9650), '\x20', chr(9688)],
                                      [chr(9688), '\x20', '\x20', '\x23', chr(9688)],
                                      [chr(9688), chr(9688), chr(9688), chr(9688), chr(9688)]], (2, 2)
        assert sm.corresponding_direction('вверх', model) == 1
        assert sm.corresponding_direction('влево', model) == -1
        assert sm.corresponding_direction('qqq', model) == -1

    def test_robot_report(self):
        model = fd.field_c
        model.picture = [[chr(9688), chr(9688), chr(9688), chr(9688), chr(9688)],
                                      [chr(9688), '\x23', '\x20', '\x20', chr(9688)],
                                      [chr(9688), '\x20', chr(9650), '\x20', chr(9688)],
                                      [chr(9688), '\x20', '\x20', '\x23', chr(9688)],
                                      [chr(9688), chr(9688), chr(9688), chr(9688), chr(9688)]]
        model.robot, model.last_pos = (2, 2), ()
        assert sm.robot_report(model) == '(2,2), вверх, '
        model.robot, model.last_pos = (2, 2), (2, 3)
        assert sm.robot_report(model) == '(2,2), вверх, (3,2)'
        model.picture[model.robot[0]][model.robot[1]] = chr(9668)
        assert sm.robot_report(model) == '(2,2), влево, (3,2)'
        model.picture[model.robot[0]][model.robot[1]] = '\x23'
        assert sm.robot_report(model) == '(2,2), что-то не так, (3,2)'

    def setup_class(self):
        print("\n=== TestSimulator - setup class ===\n")

    def teardown_class(self):
        print("\n=== TestSimulator - teardown class ===\n")

    def setup(self):
        print("TestSimulator - setup method")

    def teardown(self):
        print("TestSimulator - teardown method")

class TestField:

    def test_obstacle_start(self):
        model = fd.field_c(3, 3, 1)
        model.picture, model.robot = [[chr(9688), chr(9688), chr(9688), chr(9688), chr(9688)],
                                      [chr(9688), '\x23', '\x20', '\x20', chr(9688)],
                                      [chr(9688), '\x20', chr(9650), '\x20', chr(9688)],
                                      [chr(9688), '\x20', '\x20', '\x23', chr(9688)],
                                      [chr(9688), chr(9688), chr(9688), chr(9688), chr(9688)]], (2, 2)
        assert model.obstacle_start() != (2, 2)

    def test_move_point(self):
        model = fd.field_c(5, 5, 1)
        model.picture, model.robot = [[chr(9688), chr(9688), chr(9688), chr(9688), chr(9688)],
                                      [chr(9688), '\x23', '\x20', '\x20', chr(9688)],
                                      [chr(9688), '\x20', chr(9650), '\x20', chr(9688)],
                                      [chr(9688), '\x20', '\x20', '\x23', chr(9688)],
                                      [chr(9688), chr(9688), chr(9688), chr(9688), chr(9688)]], (2, 2)
        assert model.move_point(0, [2, 2]) == [2, 1]
        assert model.move_point(123, [2, 2]) == -1

    def test_free_place_check(self):
        model = fd.field_c(5, 5, 1)
        model.picture, model.robot = [[chr(9688), chr(9688), chr(9688), chr(9688), chr(9688)],
                                      [chr(9688), '\x23', '\x23', '\x20', chr(9688)],
                                      [chr(9688), '\x20', chr(9650), '\x20', chr(9688)],
                                      [chr(9688), '\x20', '\x20', '\x23', chr(9688)],
                                      [chr(9688), chr(9688), chr(9688), chr(9688), chr(9688)]], (2, 2)
        model.storage.append(([model.robot[0], model.robot[1]], [0, 0, 0, 0]))
        model.free_place_check(0, 0, model.storage[0][0])
        assert model.storage[0][1][0] == -1
        model.free_place_check(0, 2, model.storage[0][0])
        assert model.storage[0][1][2] == 1

    def test_center_search(self):
        model = fd.field_c(5, 5, 1)
        assert model.center_search(1) == 1
        assert model.center_search(2) == 1
        assert model.center_search(3) == 2
        assert model.center_search(22.2) == -1
        assert model.center_search('111') == -1

    def test_place_robot(self):
        model = fd.field_c(12.1, 5, 1)
        example = [[chr(9688), chr(9688), chr(9688), chr(9688), chr(9688)],
                                      [chr(9688), '\x23', '\x23', '\x20', chr(9688)],
                                      [chr(9688), '\x20', chr(9650), '\x20', chr(9688)],
                                      [chr(9688), '\x20', '\x20', '\x23', chr(9688)],
                                      [chr(9688), chr(9688), chr(9688), chr(9688), chr(9688)]]
        model.picture = example
        assert model.place_robot() == -1
        model = fd.field_c('qqq', 5, 1)
        model.picture = example
        assert model.place_robot() == -1
        model = fd.field_c(3, 3, 1)
        model.picture = example
        assert model.place_robot() == 0


    def setup_class(self):
        print("\n=== TestField - setup class ===\n")

    def teardown_class(self):
        print("\n=== TestField - teardown class ===\n")

    def setup(self):
        print("TestField - setup method")

    def teardown(self):
        print("TestField - teardown method")